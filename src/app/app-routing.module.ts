import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExchangerComponent } from './components/exchanger/exchanger.component';

const routes: Routes = [
  {
    path: 'exchanger_test_route', component: ExchangerComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],

  exports: [RouterModule]

})
export class AppRoutingModule { }
