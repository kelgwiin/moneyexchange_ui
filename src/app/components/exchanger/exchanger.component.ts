import { Component, OnInit } from '@angular/core';
import {ExchangerService} from '../../services/exchanger.service';

import { timeInterval, map } from 'rxjs/operators';

import { Observable} from 'rxjs';
import { timer } from 'rxjs';


@Component({
  selector: 'app-exchanger',
  templateUrl: './exchanger.component.html',
  styleUrls: ['./exchanger.component.scss']
})
export class ExchangerComponent implements OnInit {
  private usdValue: number;
  private eurRate: number;
  private resultEur: number;
  private value = '';
  private delayTimeMs: number;

  constructor(private exchangerService: ExchangerService) {
    this.delayTimeMs = 600000;
  }

  ngOnInit() {
    timer(0, this.delayTimeMs).subscribe(t => {
      this.setRate();
    });

  }

  setRate() {
    this.exchangerService.getEurRate()
    .subscribe(eurRate => {
      this.eurRate = eurRate;
      console.log(`Rate EUR: ${eurRate}`);
    });
  }

  onEnter(value: String) {
    this.setRate();
  }

  calculate(): any {
    console.log('usd value ' + this.usdValue);
    if (this.usdValue !== undefined && this.usdValue !== null) {
      return this.usdValue * this.eurRate;
    } else {
      return 'EUR';
    }
  }
}
