import { TestBed, inject } from '@angular/core/testing';

import { ExchangerService } from './exchanger.service';

describe('ExchagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExchangerService]
    });
  });

  it('should be created', inject([ExchangerService], (service: ExchangerService) => {
    expect(service).toBeTruthy();
  }));
});
