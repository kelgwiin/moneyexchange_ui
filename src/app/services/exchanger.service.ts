import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Contants} from '../util/Constants';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

const BASE_URL = Contants.SERVER_IP + '/exchanger/latest';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class ExchangerService {
  private urlEurRate = BASE_URL + '?base=USD&symbols=EUR';
  constructor(
    private http: HttpClient
  ) { }

  getEurRate(): Observable<number> {
    return this.http.get<number>(this.urlEurRate, httpOptions);
  }
}
